import axios from 'axios'
import api from './api'
import getHeaders from './header'

let instance = axios.create({
  timeout: 1000 * 12
})

export default function (name, hasFile, parameter) {

  let headers = getHeaders()

  if (hasFile === true) {
    let data = new FormData()
    headers['Content-Type'] = 'multipart/form-data'
    for (var index in parameter) {
      data.append(index, parameter[index])
    }
    return instance.post(api[name], data,
      {
        headers: headers
      }).then(response => {
      console.log(response)
      return response.data
    })
  } else {
    headers['Content-Type'] = 'application/json'
    return instance.post(api[name], parameter,
      {
        headers: headers
      }).then(response => {
      if (response.headers['user-id']) {
        localStorage.setItem('user-id', response.headers['user-id'])
        localStorage.setItem('user-token', response.headers['user-token'])
      }
      return response.data
    })
  }
}
