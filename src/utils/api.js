export default {
  login: 'http://www.school.com/login', // 登录接口
  registered: 'http://www.school.com/register', // 注册接口
  release: 'http://www.school.com/release', // 发布二手商品信息
  uploads: 'http://www.school.com/uploads', // 发布二手商品图片
  addtype: 'http://www.school.com/addtype', // 添加商品类型
  releasewantbuy: 'http://www.school.com/releasewantbuy', // 发布求购商品信息
  wantBuyList: 'http://www.school.com/wantBuyList', // 获取求购信息表
  earntype: 'http://www.school.com/earntype', // 获取商品类型
  goodslist: 'http://www.school.com/goodsList', // 请求商品的数据列表
  gooslistdetail: 'http://www.school.com/goodsListDetail', // 查询商品的详情
  // 管理员-->用户管理
  earnUserList: 'http://www.school.com/earnuserlist', // 获取所有的用户数据
  deleteUser: 'http://www.school.com/deleteUser', // 删除一个用户
  EarnOneUserResponse: 'http://www.school.com/EarnOneUserResponse', // 获取到一条用户数据
  updateUser: 'http://www.school.com/updateUser', // 更新用户数据
  //  管理员分类管理相关
  deleteType: 'http://www.school.com/deleteType', // 删除一个分类
  updateType: 'http://www.school.com/updateType', // 更新分类信息
  // 二手商品管理
  updateGoods: 'http://www.school.com/updateGoods', // 更新商品信息
  deleteGoods: 'http://www.school.com/deleteGoods', // 删除商品
  // 二手求购管理
  deleteWant: 'http://www.school.com/deleteWant', // 删除信息
  updateWant: 'http://www.school.com/updateWant' // 更新信息
}
