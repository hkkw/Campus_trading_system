export default function getHeaders () {
  let headers = {}
  if (localStorage.getItem('user-id') !== null) {
    headers['user-id'] = localStorage.getItem('user-id');
    headers['user-token'] = localStorage.getItem('user-token');
  }
  return headers
}
