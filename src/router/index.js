/* eslint-disable no-undef */
import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/pages/Index'
import Login from '@/pages/Login'
import CampusSecond from '@/pages/CampusSecond'
import ReleasesGoods from '@/pages/CampusSecond/ReleasesGoods'
import ShopDetail from '@/pages/CampusSecond/ShopDetail'
import CampusWantBuy from '@/pages/CampusWantBuy'
import ReleaseWantBuy from '@/pages/ReleaseWantBuy'
import Jion from '@/pages/Jion'
import Preson from '@/pages/Person'
import Registered from '@/pages/Registered'
import ReleaseSucess from '@/pages/Registered/ReleaseSucess'
import Findpwd from '@/pages/Findpwd'
import Test from '@/pages/Test'
import Test2 from '@/pages/Test/Test2'
import Test3 from '@/pages/Test/Test3'
import Test4 from '@/pages/Test/Test4'
import IndexAdmin from '@/pages/admin/goodstype/Index'
import Updata from '@/pages/admin/goodstype/Updata'
import AddType from '@/pages/admin/goodstype/AddType'
import AdminIndex from '@/pages/admin/user/index'
import GoodsControl from '@/pages/admin/goodsControl/goodsControl'
import ReleaseControl from '@/pages/admin/ReleaseControl/ReleaseControl'
Vue.use(Router)

export default new Router({
  routes: [{
    path: '/releaseControl', // 二手求购管理页面
    name: 'ReleaseControl',
    component: ReleaseControl
  }, {
    path: '/goodsControl', // 商品管理页面
    name: 'GoodsControl',
    component: GoodsControl
  }, {
    path: '/', // 网站首页
    name: 'index',
    component: Index
  }, {
    path: '/login', // 登录
    name: 'login',
    component: Login
  }, {
    path: '/campussecond', // 二手市场
    name: 'campussecond',
    component: CampusSecond
  }, {
    path: '/campuswantbuy', // 二手求购
    name: 'campuswantbuy',
    component: CampusWantBuy
  }, {
    path: '/jion', // 加入我们
    name: 'jion',
    component: Jion
  }, {
    path: '/test', // 测试
    name: 'test',
    component: Test
  }, {
    path: '/shopdetail', // 二手市场商品详情
    name: 'shopdetail',
    component: ShopDetail
  }, {
    path: '/releasewantbuy', // 发布求购信息
    name: 'releasewantbuy',
    component: ReleaseWantBuy
  }, {
    path: '/person', // 个人中心
    name: 'person',
    component: Preson
  }, {
    path: '/registered', // 注册
    name: 'registered',
    component: Registered
  }, {
    path: '/releasesucess', // 注册成功
    name: 'releasesucess',
    component: ReleaseSucess
  }, {
    path: '/findpwd', // 找回密码
    name: 'Findpwd',
    component: Findpwd
  }, {
    path: '/test2', // 测试vue
    name: 'test2',
    component: Test2
  }, {
    path: '/releasesgoods', // 发布二手商品
    name: 'releasesgoods',
    component: ReleasesGoods
  }, {
    path: '/indexadmin', // 发布二手商品
    name: 'indexadmin',
    component: IndexAdmin
  }, {
    path: '/updata', // 发布二手商品
    name: 'updata',
    component: Updata
  }, {
    path: '/addtype', // 发布二手商品
    name: 'addtype',
    component: AddType
  }, {
    path: '/test3', // 测试vue
    name: 'test3',
    component: Test3
  }, {
    path: '/test4', // 测试vue
    name: 'test4',
    component: Test4
  }, {
    path: '/AdminIndex', // 测试vue
    name: 'AdminIndex',
    component: AdminIndex
  }]
})
